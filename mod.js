const
    canBeJsonString = str => str.startsWith('{') && str.endsWith('}') || str.startsWith('[') && str.endsWith(']'),
    toValue = _ => {
        const [[type, value]] = Object.entries(_);
        switch(type){
            case 'stringValue': {
                return canBeJsonString(value)
                    ? JSON.parse(value.toString())
                    : Number(value) || value;
            }
            case 'longValue':
            case 'doubleValue':
            case 'booleanValue':
                return value;
            case 'blobValue': {
                const stringValue = value.toString();
                return canBeJsonString(stringValue)
                    ? JSON.parse(stringValue)
                    : stringValue;
            }
            case 'isNull': return null;
        }
    };

/**
 * @param {function} executeStatement
 * @returns {function}
 */
module.exports = ({
    executeStatement
}) => /**
     * @param {string} sql
     * @param {object} [data]
     * @returns {{abort: function, promise: function}}
     */
    ({
    sql,
    data = {}
}) => {
    const request = executeStatement({
        sql,
        parameters: Object.entries(data).map(([name, value]) => {
            const type = typeof value;
            switch(type){
                case 'string':
                case 'boolean': return {
                    name,
                    value: { [`${type}Value`]: value }
                };
                case 'number': return {
                    name,
                    value: Number.isInteger(value)
                        ? { longValue: value }
                        : { doubleValue: value }
                };
                case 'object': return {
                    name,
                    value: value === null
                        ? { isNull: true }
                        : { stringValue: JSON.stringify(value) }
                };
            }
        }),
        includeResultMetadata: true
    });
    return {
        /** @returns {void} */
        abort: () => request.abort(),
        /** @returns {Promise<{generatedFields: *[], records: *[][], columns: {name: string, label: string, type: string}[], updatedRecordCount: number}>} */
        promise: async () => {
            const {
                columnMetadata = [],
                generatedFields = [],
                numberOfRecordsUpdated = 0,
                records = []
            } = await request.promise();
            return {
                columns: columnMetadata.map(item => ({
                    name: item.name,
                    label: item.label,
                    type: item.typeName
                })),
                generatedFields: generatedFields.map(toValue),
                updatedRecordCount: numberOfRecordsUpdated,
                records: records.map(record => record.map(toValue))
            };
        }
    };
};